# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: mjalenqu <mjalenqu@student.le-101.fr>      +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2019/02/25 12:55:34 by mjalenqu     #+#   ##    ##    #+#        #
#    Updated: 2019/11/05 15:04:16 by mjalenqu    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

.PHONY : all clean fclean re

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME = libft_malloc_$(HOSTTYPE).so
LINK = libft_malloc.so

BLUE=\033[0;38;5;123m
LIGHT_PINK = \033[0;38;5;200m
PINK = \033[0;38;5;198m
DARK_BLUE = \033[1;38;5;110m
GREEN = \033[1;32;111m
LIGHT_GREEN = \033[1;38;5;121m
LIGHT_RED = \033[0;38;5;110m
FLASH_GREEN = \033[33;32m
WHITE_BOLD = \033[37m
YELLOW = \033[1;33m
RED = \033[1;31m

# **************************************************************************** #
#									PATH                                       #
# **************************************************************************** #

SRC_PATH = ./src/
OBJ_PATH = ./obj/
INC_PATH = ./includes/
LIB_PATH = ./libft/

# **************************************************************************** #
# 									SRCS                                       #
# **************************************************************************** #

INC_NAME = ft_malloc.h
SRC_NAME =	ft_malloc.c \
			malloc_allocation.c \
			malloc_init.c \
			malloc_utils.c \
			malloc_list.c \
			malloc_show_alloc_mem.c \
			ft_free.c \
			ft_realloc.c \
			malloc_free_find.c

# **************************************************************************** #
#  									VAR                                        #
# **************************************************************************** #

OBJ_NAME = $(SRC_NAME:.c=.o)
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_NAME))
INC = $(addprefix $(INC_PATH), $(INC_NAME))

# **************************************************************************** #
#  									FLAG                                       #
# **************************************************************************** #

FLAGS = -Wall -Werror -Wextra -g3 #-fsanitize=address#-fsanitize=undefined 

# **************************************************************************** #
# 									REGLES                                     #
# **************************************************************************** #

all : 
	@make -C $(LIB_PATH)
	@make $(NAME)

$(NAME): $(OBJ) Makefile $(INC)
	@gcc $(FLAGS) -shared -o $(NAME) $(OBJ) $(LIB_PATH)libft.a
	@echo "	\033[2K\r$(DARK_BLUE)malloc lib:	$(GREEN)loaded\033[0m"
	@rm -rf $(LINK)
	@ln -s $(NAME) $(LINK)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(INC)
	@if test ! -d $(dir $@); then mkdir -p $(dir $@); fi
	@gcc $(FLAGS) -I $(INC_PATH) -o $@ -c $< -fPIC
	@printf "\033[2K\r$(DARK_BLUE)malloc lib:	\033[37m$<\033[36m \033[0m"

clean:
	@make -C $(LIB_PATH) clean
	@rm -rf $(OBJ_PATH)
	@echo "$(YELLOW)ft_malloc:	$(RED)object files deleted$(RESET)"

fclean: clean
	@make -C $(LIB_PATH) fclean
	@rm -rf $(NAME)
	@rm -rf $(LINK)
	@echo "$(YELLOW)ft_malloc:	$(RED)library file deleted$(RESET)"


re: fclean all
