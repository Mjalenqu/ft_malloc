#ifndef FT_MALLOC_H
# define FT_MALLOC_H

# include "../libft/includes/linked_header.h"
# include <sys/mman.h>
# include <unistd.h>
# include <sys/resource.h>
# include <sys/time.h>
# include <stdio.h>
# include <string.h>

# define TINY_SIZE 128
# define SMALL_SIZE 1024
# define MIN_ELEM_ALLOC 100
# define TINY_MODE 0
# define SMALL_MODE 1
# define LARGE_MODE 2

typedef struct	s_malloc
{
	struct s_malloc_tiny		*tiny;
	struct s_malloc_tiny		*tiny_head;
	struct s_malloc_small		*small;
	struct s_malloc_small		*small_head;
	struct s_malloc_large		*large;
	struct s_malloc_large		*large_head;
}				t_malloc;

typedef struct	s_malloc_tiny
{
	void						*ptr;
	int							index;
	struct s_malloc_tiny		*next;
	struct s_malloc_tiny		*prev;
	unsigned short				size[MIN_ELEM_ALLOC];
}				t_malloc_tiny;

typedef struct	s_malloc_small
{
	void						*ptr;
	int							index;
	struct s_malloc_small		*next;
	struct s_malloc_small		*prev;
	unsigned short				size[MIN_ELEM_ALLOC];
}				t_malloc_small;

typedef struct	s_malloc_large
{
	void						*ptr;
	size_t						size;
	struct s_malloc_large		*next;
	struct s_malloc_large		*prev;
}				t_malloc_large;

extern t_malloc *g_malloc;

/*
**┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
**┃                            Functions									   ┃
**┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/

/*
** FT_MALLOC.C
*/

void			*malloc(size_t size);
void			free(void *ptr);
void			*realloc(void *ptr, size_t size);
void			show_alloc_mem(void);

/*
** MALLOC_INIT.C
*/

t_malloc_tiny	*init_tiny_struct();
t_malloc_small	*init_small_struct();
t_malloc_large	*init_large_struct(size_t size);
int				init_g_malloc();

/*
** MALLOC_ALLOCATION.C
*/

void			*allocate_tiny(size_t size);
void			*allocate_small(size_t size);
void			*allocate_large(size_t size);

/*
** MALLOC_UTILS.C
*/

int				check_small_space(t_malloc_small *small, int *i);
int				check_tiny_space(t_malloc_tiny *tiny, int *i);

/*
** MALLOC_LIST.C
*/

t_malloc_tiny	*go_to_page_tiny(t_malloc_tiny *head, int page);
t_malloc_small	*go_to_page_small(t_malloc_small *head, int page);
int				listlen_tiny(t_malloc_tiny *head);
int				listlen_small(t_malloc_small *head);

/*
** MALLOC_SHOW_ALLOC_MEM.C
*/

void			print_tiny_mem(t_malloc_tiny *tiny);
void			print_small_mem(t_malloc_small *small);
void			print_large_mem(t_malloc_large *large);
void			ft_show_alloc_mem(void);
void			putnbr_base(unsigned long n, unsigned long base);
/*
** FT_FREE.C
*/
int				delete_tiny(void *ptr, t_malloc_tiny *tiny,
				t_malloc_tiny *to_free);
void			check_tiny();
int				delete_small(void *ptr, t_malloc_small *small,
				t_malloc_small *to_free);
void			check_small();
void			ft_free(void *ptr);

/*
** MALLOC_FREE_FIND.C
*/

int				find_in_small(void *ptr);
int				find_in_tiny(void *ptr);
int				find_in_large(void *ptr, t_malloc_large *large,
				t_malloc_large *to_free);

/*
** FT_REALLOC.C
*/

int				get_size_small(void *ptr);
int				get_size_tiny(void *ptr);
int				get_size_large(void *ptr);
int				check_size(void *ptr);
void			*ft_realloc(void *ptr, size_t size);

#endif
