#include "includes/ft_malloc.h"

int print_g_malloc()
{
    if (g_malloc == NULL)
        return (printf("ptr = %p\n", g_malloc));
    return (printf("ptr = %p -- tiny = %p -- small = %p -- large = %p\n", g_malloc, g_malloc->tiny, g_malloc->small, g_malloc->large));
}

int print_tiny()
{
    t_malloc_tiny *tiny;
    int page;
    int i;

    printf("------------------------\nimpression TINY\n");
    i = 0;
    page = 0;
    tiny = g_malloc->tiny_head;
    while (tiny)
    {
        printf("NOUVELLE PAGE: t_malloc_tiny = %p\n", tiny);
        i = 0;
        while (i < MIN_ELEM_ALLOC)
        {
            printf("page %d, index %d, ptr = %p\n", page, i, tiny->ptr + (i * TINY_SIZE));
            // if (tiny->size[i] != 0)
            // {
            // 	printf("page %d, index %d, ptr = %p\n", page, i, tiny->ptr + (i * TINY_SIZE));
            // }
            ++i;
        }
        tiny = tiny->next;
        page++;
    }
    return (-1);
}

int print_small()
{
    t_malloc_small *small;
    int page;
    int i;

    printf("------------------------\nimpression small\n");
    i = 0;
    page = 0;
    small = g_malloc->small_head;
    while (small)
    {
        printf("NOUVELLE PAGE: t_malloc_small = %p\n", small);
        i = 0;
        while (i < MIN_ELEM_ALLOC)
        {
            printf("page %d, index %d, ptr = %p, taille = %d\n", page, i, small->ptr + (i * SMALL_SIZE), small->size[i]);
            // if (small->size[i] != 0)
            // {
            // }
            ++i;
        }
        small = small->next;
        page++;
    }
    return (-1);
}

void print_large()
{
    printf("-----------------\nlecture de la liste large\n");
    if (g_malloc->large_head == NULL)
        return;
    printf("ptr large head = %p\n", g_malloc->large_head->ptr);
    g_malloc->large = g_malloc->large_head;
    while (g_malloc->large)
    {
        printf("maillon ptr = %p\n", g_malloc->large->ptr);
        g_malloc->large = g_malloc->large->next;
    }
}

void print_tab(unsigned short tab[])
{
    int i = 0;
    while (i < 100)
    {
        printf("index : %d\tsize = %d\n", i, tab[i]);
        i++;
    }
}

int main()
{
    int i;
    char *test_monmalloc1;
    char *test_monmalloc2;
    char *test_monmalloc3;
    char *test_monmalloc4;
    char *test_malloc1;
    char *test_malloc2;
    char *string = "123456";
    int taille_test = 50;

    // show_alloc_mem();
    ft_putstr("début \n");
    printf(" >ok< \n");
    printf("taille de getpagesize ->> %d\n", getpagesize());
    printf("ta race\n");
    ft_putstr("passe\n");

    // print_small();
    // print_tiny();
    // print_large();
    // ft_printf("{B.U.}ca marche!\n");
    i = 0;
    // test_malloc1 = malloc(taille_test);
    // test_malloc2 = malloc(taille_test);
    test_monmalloc1 = malloc(taille_test);
    printf("add1 = %p\n", test_monmalloc1);
    test_monmalloc2 = malloc(taille_test);
    printf("add2 = %p\n", test_monmalloc2);
    test_monmalloc3 = malloc(taille_test);
    printf("add3 = %p\n", test_monmalloc3);
    test_monmalloc4 = malloc(taille_test);
    printf("add4 = %p\n", test_monmalloc4);

    printf("-----------------\ntiny_head ptr = %p\n", g_malloc->tiny_head->ptr);
    print_tiny();
    free(test_monmalloc2);
    printf("-----------------\ntiny_head ptr = %p\n", g_malloc->tiny_head->ptr);
    print_tiny();
    test_monmalloc2 = malloc(taille_test);
    printf("-----------------\ntiny_head ptr = %p\n", g_malloc->tiny_head->ptr);
    print_tiny();
    puts("--------------------------------------------------");
    if (g_malloc->large_head)
    {
        printf("ptr large head ptr = %p\n", g_malloc->large_head->ptr);
        printf("\nlecture de la liste large\n");
        g_malloc->large = g_malloc->large_head;
        while (g_malloc->large)
        {
            printf("maillon ptr = %p\n", g_malloc->large->ptr);
            g_malloc->large = g_malloc->large->next;
        }
    }
    else
        printf("ptr large head ptr = %p\n", g_malloc->large_head);
    return (0);
}