#include "../includes/ft_malloc.h"
#include <assert.h>

int		delete_tiny(void *ptr, t_malloc_tiny *tiny, t_malloc_tiny *to_free)
{
	tiny = g_malloc->tiny_head;
	if (tiny->ptr == ptr)
	{
		g_malloc->tiny_head = tiny->next;
		if (g_malloc->tiny == tiny)
			g_malloc->tiny = tiny->next;
		if (munmap(tiny->ptr, TINY_SIZE * MIN_ELEM_ALLOC) == -1)
			return (-1);
		return (munmap(tiny, sizeof(tiny)));
	}
	while (tiny)
	{
		if (tiny->next && tiny->next->ptr == ptr)
		{
			to_free = tiny->next;
			tiny->next = tiny->next->next;
			if (g_malloc->tiny == to_free)
				g_malloc->tiny = tiny->next == NULL ? tiny : tiny->next;
			if (munmap(to_free->ptr, TINY_SIZE * MIN_ELEM_ALLOC) == -1)
				return (-1);
			return (munmap(to_free, sizeof(tiny)));
		}
		tiny = tiny->next;
	}
	return (-1);
}

void	check_tiny(void)
{
	t_malloc_tiny	*tiny;
	t_malloc_tiny	*tmp;
	int				i;
	int				check;

	tiny = g_malloc->tiny_head;
	while (tiny)
	{
		i = -1;
		check = 0;
		while (++i < MIN_ELEM_ALLOC)
			if (tiny->size[i] != 0)
			{
				check = 1;
				break ;
			}
		if (!check && tiny != g_malloc->tiny_head)
		{
			tmp = tiny->next;
			delete_tiny(tiny->ptr, NULL, NULL);
			tiny = tmp;
		}
		else
			tiny = tiny->next;
	}
}

int		delete_small(void *ptr, t_malloc_small *small, t_malloc_small *to_free)
{
	small = g_malloc->small_head;
	if (small->ptr == ptr)
	{
		g_malloc->small_head = small->next;
		if (g_malloc->small == small)
			g_malloc->small = small->next;
		if (munmap(small->ptr, SMALL_SIZE * MIN_ELEM_ALLOC) == -1)
			return (-1);
		return (munmap(small, sizeof(small)));
	}
	while (small)
	{
		if (small->next && small->next->ptr == ptr)
		{
			to_free = small->next;
			small->next = small->next->next;
			if (g_malloc->small == to_free)
				g_malloc->small = small->next == NULL ? small : small->next;
			if (munmap(to_free->ptr, SMALL_SIZE * MIN_ELEM_ALLOC) == -1)
				return (-1);
			return (munmap(to_free, sizeof(small)));
		}
		small = small->next;
	}
	return (-1);
}

void	check_small(void)
{
	t_malloc_small	*small;
	t_malloc_small	*tmp;
	int				i;
	int				check;

	small = g_malloc->small_head;
	while (small)
	{
		i = -1;
		check = 0;
		while (++i < MIN_ELEM_ALLOC)
			if (small->size[i] != 0)
			{
				check = 1;
				break ;
			}
		if (!check && small != g_malloc->small_head)
		{
			tmp = small->next;
			delete_small(small->ptr, NULL, NULL);
			small = tmp;
		}
		else
			small = small->next;
	}
}

void	ft_free(void *ptr)
{
	// write(1, "2\n", 2);
	// ft_putstr("Free de 0x");
	// putnbr_base((uintptr_t)ptr, 16);
	// ft_putstr("\n");
	if (ptr == NULL)
		return ;
	// write(1, "3\n", 2);
	if (find_in_tiny(ptr) == 1)
	{
		check_tiny();
		// write(1, "4\n", 2);
	}
	else if (find_in_small(ptr) == 1)
	{
		check_small();
		// write(1, "5\n", 2);
	}
	else if (g_malloc->large_head)
	{
		find_in_large(ptr, NULL, NULL);
		// write(1, "6\n", 2);
	}
}
