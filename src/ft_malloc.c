#include "../includes/ft_malloc.h"

t_malloc *g_malloc = NULL;


void	*malloc(size_t size)
{
	void	*ret;
	int		size_aligned;

	ret = NULL;
	// putnbr_base(size, 10);
	// write(1, "\n", 1);
	if (g_malloc == NULL)
	{
		if (init_g_malloc() == 0)
			return (NULL);
	}
	size_aligned = (((size - 1) >> 3) << 3) + 8;
	if (size_aligned <= TINY_SIZE)
	{
		ret = allocate_tiny(size);
	}
	else if (size_aligned <= SMALL_SIZE)
	{
		ret = allocate_small(size);
	}
	else
	{
		ret = allocate_large(size);
	}
	// write(1, "0x", 2);
	// putnbr_base((uintptr_t)ret, 16);
	// write(1, "\n", 1);
	// show_alloc_mem();
	// ft_putstr("\n\n");
	return (ret);
}

void	free(void *ptr)
{
	// write(1, "1\n", 2);
	ft_free(ptr);
	// write(1, "--------------------\n", 21);
}

void	*realloc(void *ptr, size_t size)
{
	// ft_putstr("realloc\n");
	return (ft_realloc(ptr, size));
}

void	show_alloc_mem(void)
{
	ft_show_alloc_mem();
}
