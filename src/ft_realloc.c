#include "../includes/ft_malloc.h"

int		get_size_small(void *ptr)
{
	t_malloc_small	*small;
	int				i;

	small = g_malloc->small_head;
	while (small)
	{
		i = 0;
		while (i < MIN_ELEM_ALLOC)
		{
			if (small->ptr + (i * SMALL_SIZE) == ptr)
				return (small->size[i]);
			++i;
		}
		small = small->next;
	}
	return (-1);
}

int		get_size_tiny(void *ptr)
{
	t_malloc_tiny	*tiny;
	int				i;

	tiny = g_malloc->tiny_head;
	while (tiny)
	{
		i = 0;
		while (i < MIN_ELEM_ALLOC)
		{
			if (tiny->ptr + (i * TINY_SIZE) == ptr)
				return (tiny->size[i]);
			++i;
		}
		tiny = tiny->next;
	}
	return (-1);
}

int		get_size_large(void *ptr)
{
	t_malloc_large *large;

	large = g_malloc->large_head;
	while (large)
	{
		if (large->ptr == ptr)
			return (large->size);
		large = large->next;
	}
	return (-1);
}

int		check_size(void *ptr)
{
	int cpy_size;

	cpy_size = get_size_tiny(ptr);
	// ft_putstr("\ncpy_size -> ");
	// putnbr_base(cpy_size, 10);
	// ft_putstr("\n");
	if (cpy_size > -1)
		return (cpy_size);
	cpy_size = get_size_small(ptr);
	// ft_putstr("passage\n");
	// ft_putstr("\ncpy_size -> ");
	// putnbr_base(cpy_size, 10);
	// ft_putstr("\n");
	if (cpy_size > -1)
		return (cpy_size);
	return (get_size_large(ptr));
}

void	*ft_realloc(void *ptr, size_t size)
{
	int		cpy_size;
	void	*new;


	// ft_putstr("REALLOC: 0x");
	// putnbr_base((uintptr_t)ptr, 16);
	cpy_size = check_size(ptr);
	// ft_putstr(" /// new_size -> \n");
	// putnbr_base(size, 10);
	// ft_putstr("\nCopy size = ");
	// putnbr_base(cpy_size, 10);
	// ft_putstr("\n");
	if (!ptr && size > 0) {
		return malloc(size);
	}
	if (size == 0 && ptr)
	{
		// ft_putstr("size == 0 && ptr");
		free(ptr);
		return (NULL);
	}
	if (cpy_size == -1)
	{
		// ft_putstr("Invalid pointer!\n");
		return (malloc(size));
	}
	if (size == 0)
	{
		// ft_putstr("size == 0\n");
		free(ptr);
		return (NULL);
	}
	if ((size_t)cpy_size > size)
		cpy_size = size;
	if ((new = malloc(size)) == NULL) {
		// ft_putstr("malloc = null");
		return (NULL);
	}
	new = ft_memcpy(new, ptr, cpy_size);
	free(ptr);
	// ft_putstr("fin de realloc\n");
	return (new);
}
