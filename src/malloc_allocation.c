#include "../includes/ft_malloc.h"

void	*allocate_tiny(size_t size)
{
	int				i;
	int				page;
	t_malloc_tiny	*elem;

	i = 0;
	page = 0;
	if ((page = check_tiny_space(g_malloc->tiny_head, &i)) == -1)
	{
		elem = init_tiny_struct();
		if (g_malloc->tiny_head == NULL)
			g_malloc->tiny_head = elem;
		else
			g_malloc->tiny->next = elem;
		g_malloc->tiny = elem;
		page = listlen_tiny(g_malloc->tiny_head);
		i = 0;
	}
	elem = go_to_page_tiny(g_malloc->tiny_head, page);
	elem->size[i] = size;
	return (elem->ptr + (i * TINY_SIZE));
}

void	*allocate_small(size_t size)
{
	int				i;
	int				page;
	t_malloc_small	*elem;

	i = 0;
	page = 0;
	if ((page = check_small_space(g_malloc->small_head, &i)) == -1)
	{
		elem = init_small_struct();
		if (g_malloc->small_head == NULL)
			g_malloc->small_head = elem;
		else
			g_malloc->small->next = elem;
		g_malloc->small = elem;
		page = listlen_small(g_malloc->small_head);
		i = 0;
	}
	elem = go_to_page_small(g_malloc->small_head, page);
	elem->size[i] = size;
	return (elem->ptr + (i * SMALL_SIZE));
}

void	*allocate_large(size_t size)
{
	t_malloc_large *new;

	new = init_large_struct(size);
	if (g_malloc->large == NULL)
	{
		g_malloc->large = new;
		g_malloc->large_head = new;
		g_malloc->large->prev = g_malloc->large_head;
	}
	else
	{
		g_malloc->large->next = new;
		g_malloc->large = g_malloc->large->next;
		g_malloc->large->prev = g_malloc->large_head;
	}
	return (new->ptr);
}
