#include "../includes/ft_malloc.h"

int		find_in_large(void *ptr, t_malloc_large *large, t_malloc_large *to_free)
{
	large = g_malloc->large_head;
	if (large->ptr == ptr)
	{
		g_malloc->large_head = large->next;
		if (g_malloc->large == large)
			g_malloc->large = large->next;
		if (munmap(large->ptr, large->size) == -1)
			return (-1);
		return (munmap(large, sizeof(large)));
	}
	while (large)
	{
		if (large->next && large->next->ptr == ptr)
		{
			to_free = large->next;
			large->next = large->next->next;
			if (g_malloc->large == to_free)
				g_malloc->large = large->next;
			if (munmap(to_free->ptr, to_free->size) == -1)
				return (-1);
			return (munmap(to_free, sizeof(large)));
		}
		large = large->next;
	}
	return (-1);
}

int		find_in_tiny(void *ptr)
{
	t_malloc_tiny	*tiny;
	int				i;

	tiny = g_malloc->tiny_head;
	while (tiny)
	{
		i = 0;
		while (i < MIN_ELEM_ALLOC)
		{
			if (tiny->size[i] != 0 && (tiny->ptr + (i * TINY_SIZE)) == ptr)
			{
				tiny->size[i] = 0;
				return (1);
			}
			++i;
		}
		tiny = tiny->next;
	}
	return (-1);
}

int		find_in_small(void *ptr)
{
	t_malloc_small	*small;
	int				i;

	small = g_malloc->small_head;
	while (small)
	{
		i = 0;
		while (i < MIN_ELEM_ALLOC)
		{
			if (small->size[i] != 0 && (small->ptr + (i * SMALL_SIZE)) == ptr)
			{
				small->size[i] = 0;
				return (1);
			}
			++i;
		}
		small = small->next;
	}
	return (-1);
}
