#include "../includes/ft_malloc.h"


int				get_tiny_and_small_size(size)
{
	int ret = MIN_ELEM_ALLOC;

	while (1) 
	{
		if ((ret * size ) % getpagesize() == 0)
			return ret * size;
		else
			ret += 1;
	}
}


t_malloc_tiny	*init_tiny_struct(void)
{
	t_malloc_tiny	*elem;
	int				i;
	int				size_aligned;

	i = 0;
	if ((elem = mmap(NULL, sizeof(t_malloc_tiny), PROT_READ | PROT_WRITE,
								MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)) == NULL)
		return (NULL);
	elem->index = 0;
	while (i < MIN_ELEM_ALLOC)
		elem->size[i++] = 0;
	size_aligned = get_tiny_and_small_size(TINY_SIZE);
	if ((elem->ptr = mmap(NULL, size_aligned,
		PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)) == NULL)
		return (NULL);
	elem->next = NULL;
	elem->prev = NULL;
	return (elem);
}

t_malloc_small	*init_small_struct(void)
{
	t_malloc_small	*elem;
	int				i;
	int				size_aligned;

	i = 0;
	if ((elem = mmap(NULL, sizeof(t_malloc_small), PROT_READ | PROT_WRITE,
								MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)) == NULL)
		return (NULL);
	elem->index = 0;
	while (i < MIN_ELEM_ALLOC)
		elem->size[i++] = 0;
	size_aligned = get_tiny_and_small_size(SMALL_SIZE);
	if ((elem->ptr = mmap(NULL, size_aligned,
		PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)) == NULL)
		return (NULL);
	elem->next = NULL;
	elem->prev = NULL;
	return (elem);
}

t_malloc_large	*init_large_struct(size_t size)
{
	t_malloc_large	*elem;
	int				size_aligned;

	size_aligned = (((size - 1) / getpagesize()) + 1) * getpagesize();
	if ((elem = mmap(NULL, sizeof(t_malloc_large), PROT_READ | PROT_WRITE,
								MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)) == NULL)
		return (NULL);
	if ((elem->ptr = mmap(NULL, size_aligned, PROT_READ | PROT_WRITE,
								MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)) == NULL)
		return (NULL);
	elem->size = size;
	elem->prev = NULL;
	elem->next = NULL;
	return (elem);
}

int				init_g_malloc(void)
{
	if ((g_malloc = mmap(NULL, sizeof(t_malloc), PROT_READ | PROT_WRITE,
								MAP_PRIVATE | MAP_ANONYMOUS, -1, 0)) == NULL)
		return (0);
	g_malloc->tiny = NULL;
	g_malloc->small = NULL;
	g_malloc->large = NULL;
	g_malloc->tiny_head = NULL;
	g_malloc->small_head = NULL;
	g_malloc->large_head = NULL;
	return (1);
}
