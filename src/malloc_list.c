#include "../includes/ft_malloc.h"

t_malloc_tiny	*go_to_page_tiny(t_malloc_tiny *head, int page)
{
	while (page--)
		head = head->next;
	return (head);
}

t_malloc_small	*go_to_page_small(t_malloc_small *head, int page)
{
	while (page--)
		head = head->next;
	return (head);
}

int				listlen_tiny(t_malloc_tiny *head)
{
	int	ret;

	ret = -1;
	while (head)
	{
		head = head->next;
		ret++;
	}
	return (ret);
}

int				listlen_small(t_malloc_small *head)
{
	int	ret;

	ret = -1;
	while (head)
	{
		head = head->next;
		ret++;
	}
	return (ret);
}
