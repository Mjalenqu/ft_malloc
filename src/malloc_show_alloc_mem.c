#include "../includes/ft_malloc.h"

void	putnbr_base(unsigned long n, unsigned long base)
{
	if (n >= base)
	{
		putnbr_base(n / base, base);
		n %= base;
	}
	unsigned char c = (n >= 10) ? n + 'a' - 10 : n + '0';
	write(1, &c, 1);
}


void		print_tiny_mem(t_malloc_tiny *tiny)
{
	int		i;
	int		total;

	i = 0;
	total = 0;
	if (tiny == NULL)
		return ;
	write(1, "------------------------\nimpression tiny\n", 42);
	ft_putstr("TINY: 0x");
	putnbr_base((uintptr_t)tiny->ptr, 16);
	ft_putstr("\n");
	// ft_printf("TINY : %p\n", tiny->ptr);
	while (tiny)
	{
		while (i < MIN_ELEM_ALLOC)
		{
			if (tiny->size[i] != 0)
			{
				ft_putstr("0x");
				putnbr_base((uintptr_t)tiny->ptr + i * TINY_SIZE, 16);
				ft_putstr(" - 0x");
				putnbr_base((uintptr_t)(tiny->ptr + i * TINY_SIZE) + tiny->size[i], 16);
				ft_putstr(" : ");
				putnbr_base(tiny->size[i], 10);
				// ft_printf("%p - %p : %d octets\n", tiny->ptr + i * TINY_SIZE,
				// (tiny->ptr + i * TINY_SIZE) + tiny->size[i], tiny->size[i]);
				total += tiny->size[i];
				ft_putstr("\n");
			}
			++i;
		}
		i = 0;
		tiny = tiny->next;
	}
	ft_putstr("Total : ");
	putnbr_base(total, 10);
	ft_putstr("\n");
	// ft_printf("Total : %d\n", total);
}

void		print_small_mem(t_malloc_small *small)
{
	int		i;
	int		total;

	i = 0;
	total = 0;
	if (small == NULL)
		return ;
	write(1, "------------------------\nimpression small\n", 43);
	ft_putstr("SMALL: 0x");
	putnbr_base((uintptr_t)small->ptr, 16);
	ft_putstr("\n");
	// ft_printf("SMALL : %p\n", small->ptr);
	while (small)
	{
		while (i < MIN_ELEM_ALLOC)
		{
			if (small->size[i] != 0)
			{
				ft_putstr("0x");
				putnbr_base((uintptr_t)small->ptr + i * SMALL_SIZE, 16);
				ft_putstr(" - 0x");
				putnbr_base((uintptr_t)(small->ptr + i * SMALL_SIZE) + small->size[i], 16);
				ft_putstr(" : ");
				putnbr_base(small->size[i], 10);
				ft_putstr("\n");

				// ft_printf("%p - %p : %d octets\n", small->ptr + i * SMALL_SIZE,
				// (small->ptr + i * SMALL_SIZE) + small->size[i], small->size[i]);
				total += small->size[i];
			}
			++i;
		}
		i = 0;
		small = small->next;
	}
	ft_putstr("Total : ");
	putnbr_base(total, 10);
	ft_putstr("\n");
	// ft_printf("Total : %d\n", total);
}

void		print_large_mem(t_malloc_large *large)
{
	int		total;

	total = 0;
	if (large == NULL)
		return ;
	write(1, "------------------------\nimpression large\n", 43);
	ft_putstr("LARGE: 0x");
	putnbr_base((uintptr_t)large, 16);
	ft_putstr("\n");

	// ft_printf("LARGE : %p\n", large);
	while (large)
	{
		ft_putstr("0x");
		putnbr_base((uintptr_t)large->ptr, 16);
		ft_putstr(" - 0x");
		putnbr_base((uintptr_t)(large->ptr + large->size) + large->size, 16);
		ft_putstr(" : ");
		putnbr_base(large->size, 10);
		ft_putstr("\n");

		// ft_printf("%p - %p : %d octets\n", large->ptr,
		// large->ptr + large->size, large->size);
		total += large->size;
		large = large->next;
	}
	ft_putstr("Total : ");
	putnbr_base(total, 10);
	ft_putstr("\n");
	// ft_printf("Total : %d\n", total);
}

void		ft_show_alloc_mem(void)
{
	ft_putstr("\n____ MEMORY ALLOCATION ____\n");
	print_tiny_mem(g_malloc->tiny_head);
	print_small_mem(g_malloc->small_head);
	print_large_mem(g_malloc->large_head);
	ft_putstr("\n\n");
}
