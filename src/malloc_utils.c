#include "../includes/ft_malloc.h"

int	check_small_space(t_malloc_small *small, int *i)
{
	int	page;

	page = 0;
	while (small)
	{
		while (*i < MIN_ELEM_ALLOC)
		{
			if (small->size[*i] == 0)
			{
				return (page);
			}
			++*i;
		}
		*i = 0;
		small = small->next;
		page++;
	}
	return (-1);
}

int	check_tiny_space(t_malloc_tiny *tiny, int *i)
{
	int	page;

	page = 0;
	while (tiny)
	{
		while (*i < MIN_ELEM_ALLOC)
		{
			if (tiny->size[*i] == 0)
			{
				return (page);
			}
			++*i;
		}
		*i = 0;
		tiny = tiny->next;
		page++;
	}
	return (-1);
}
