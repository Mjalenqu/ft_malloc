#include "../includes/ft_malloc.h"

int	main(void)
{
	void	*malloc1;
	void	*malloc2;
	void	*malloc3;
	void	*malloc4;
	void	*malloc5;
	void	*malloc6;
	size_t	alloc_size;

	alloc_size = 15;
	malloc1 = malloc(alloc_size);
	malloc2 = malloc(alloc_size + 10);
	malloc3 = malloc(alloc_size + 20);
	malloc4 = malloc(1000);
	malloc5 = malloc(5000);
	malloc6 = malloc(alloc_size + 50);

	show_alloc_mem();

	free(malloc1);
	free(malloc2);
	free(malloc3);
	free(malloc4);
	free(malloc5);

	ft_putstr("=======================================================\n");
	show_alloc_mem();
}